package com.devcamp.s50.task5850.staffapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaffapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StaffapiApplication.class, args);
	}

}
