package com.devcamp.s50.task5850.staffapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5850.staffapi.interfaces.IStaffRepository;
import com.devcamp.s50.task5850.staffapi.models.CStaff;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CStaffController {
     @Autowired
     IStaffRepository pIStaffRepository;

     @GetMapping("/staff")
      public ResponseEntity<List<CStaff>> getAllProduct(){
          try{
               List<CStaff> listStaff = new ArrayList<CStaff>();
               pIStaffRepository.findAll()
               .forEach(listStaff::add);
               return new ResponseEntity<>(listStaff, HttpStatus.OK);
          }catch(Exception ex){
               return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
          }
     }
}
