package com.devcamp.s50.task5850.staffapi.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5850.staffapi.models.CStaff;

public interface IStaffRepository extends JpaRepository<CStaff, Long>{
     
}
