package com.devcamp.s50.task5850.staffapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "staff")
public class CStaff {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     private long id;    

     @Column(name = "firstname")
     private String firstname;

     @Column(name = "lastname")
     private String lastname;

     @Column(name = "email")
     private String email;

     @Column(name = "age")
     private String age;

     @Column(name = "updatedate")
     private String updateDate;

     public CStaff() {
     }

     public CStaff(long id, String firstname, String lastname, String email, String age, String updateDate) {
          this.id = id;
          this.firstname = firstname;
          this.lastname = lastname;
          this.email = email;
          this.age = age;
          this.updateDate = updateDate;
     }

     public long getId() {
          return id;
     }

     public void setId(long id) {
          this.id = id;
     }

     public String getFirstname() {
          return firstname;
     }

     public void setFirstname(String firstname) {
          this.firstname = firstname;
     }

     public String getLastname() {
          return lastname;
     }

     public void setLastname(String lastname) {
          this.lastname = lastname;
     }

     public String getEmail() {
          return email;
     }

     public void setEmail(String email) {
          this.email = email;
     }

     public String getAge() {
          return age;
     }

     public void setAge(String age) {
          this.age = age;
     }

     public String getUpdateDate() {
          return updateDate;
     }

     public void setUpdateDate(String updateDate) {
          this.updateDate = updateDate;
     }
     

     

}
